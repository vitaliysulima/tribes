var gulp = require('gulp'),
    // Для компиляции sass кода
    sass = require('gulp-sass'),
    // Синхронизация браузера
    browserSync = require('browser-sync'),
    // Шаблонизатор
    nunjucksRender = require('gulp-nunjucks-render'),
    // Создание map файла
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),

    rigger = require('gulp-rigger'),
    uglify = require('gulp-uglify');

// BrowserSync
//-----------------------------------------------------------------------------------
gulp.task('browserSync', function(){
  browserSync({
    server:{
      baseDir:'dst'
    },
    notify: false
  });
});

// Работа с Nunjucks
//-----------------------------------------------------------------------------------
gulp.task('nunjucks', function() {
  // Gets .html and .nunjucks files in pages
  return gulp.src('src/pages/**/*.+(html|nunjucks)')
  // Renders template with nunjucks
  .pipe(nunjucksRender({
      path: ['src/templates']
    }))
  // Output files in src folder
  .pipe(gulp.dest('dst'))
  .pipe(browserSync.reload({
      stream: true
  }))
});

// Работа с HTML
//-----------------------------------------------------------------------------------
gulp.task('html', function () {
    gulp.src('src/*.html')
        .pipe(rigger())
        .pipe(gulp.dest('dst'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

// Работа с SASS
//-----------------------------------------------------------------------------------
gulp.task('sass', function(){
    return gulp.src('src/sass/base.sass')
        .pipe(sourcemaps.init())
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(autoprefixer()) //Добавим вендорные префиксы
        .pipe(cleanCSS()) //Сожмем
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dst/css'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

// Работа с JS
//-----------------------------------------------------------------------------------
gulp.task('js', function(){
    return gulp.src('src/js/main.js')
        .pipe(sourcemaps.init())
        .pipe(rigger())
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dst/js'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

// Watch
//-----------------------------------------------------------------------------------
gulp.task('watch', ['browserSync', 'sass', 'nunjucks', 'js'], function(){
    gulp.watch('src/sass/**/*.sass', ['sass']);
    gulp.watch('src/pages/**/*.nunjucks', ['nunjucks']);
    gulp.watch('src/templates/**/*.nunjucks', ['nunjucks']);
    gulp.watch('src/js/**/*.js', ['js']);
});

gulp.task('ss', ['browserSync', 'sass', 'html'], function(){
    gulp.watch('src/sass/**/*.sass', ['sass']);
    gulp.watch('src/*.html', ['html']);
    gulp.watch('src/js/**/*.js', ['js']);
});
